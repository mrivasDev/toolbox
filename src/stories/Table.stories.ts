import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { TableComponent } from 'src/app/components';
import { TableModule } from 'projects/ng-utils/src/lib/table/table.module';

export default {
  title: 'Example/Table',
  component: TableComponent,
  argTypes: {
    columnsToDisplay: {
      type: 'array',
      options: []
    },
    dataSource: {
      type: 'object',
      options: {}
    },
    mobile: {
      type: 'boolean'
    },
    isLoading: {
      type: 'boolean'
    },
    filter: {
      type: 'boolean'
    }
  },
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [TableModule],
    }),
  ],
} as Meta;

const Template: Story<TableComponent> = (args: TableComponent) => ({
  component: TableComponent,
  props: args,
});

export const SimpleTable = Template.bind({});
SimpleTable.args = {
  columnsToDisplay: ['position', 'name', 'weight', 'symbol'],
  dataSource: [
    { id: 1, position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
    { id: 2, position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
    { id: 3, position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
    { id: 4, position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
    { id: 5, position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
    { id: 6, position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
    { id: 7, position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
    { id: 8, position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
    { id: 9, position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
    { id: 1, position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
  ],
  mobile: false
};
