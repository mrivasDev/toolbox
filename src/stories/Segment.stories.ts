import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { SegmentComponent } from 'src/app/components';
import { materialIcons } from 'src/utils/iconList';
import { SegmentModule } from 'projects/ng-utils/src/lib/segment/segment.module';

export default {
  title: 'Example/Segment',
  component: SegmentComponent,
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [SegmentModule],
    }),
  ],
  argTypes: {
    icon: {
      control: {
        type: 'select',
        options: materialIcons
      }
    },

    message: { control: 'text' },
    subtitle: { control: 'text' },
    actionMessage: { control: 'text' },
    actionLabel: { control: 'text' },
    actionEvent: { control: 'function' },

  },
} as Meta;

const Template: Story<SegmentComponent> = (args: SegmentComponent) => ({
  component: SegmentComponent,
  props: args,
});

export const NoDocument = Template.bind({});
NoDocument.args = {
  icon: 'description',
  message: 'There are no documents',
  actionMessage: 'You could create a new one',
  actionLabel: 'Create new document'
};
