import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { HeaderComponent } from 'src/app/components';
import { materialIcons } from 'src/utils/iconList';
import { HeaderModule } from 'projects/ng-utils/src/lib/header/header.module';
export default {
  title: 'Example/Header',
  component: HeaderComponent,
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [HeaderModule],
    }),
  ],
  argTypes: {
    icon: {
      control: {
        type: 'select',
        options: materialIcons
      }
    },
    title: { control: 'text' },
    subtitle: { control: 'text' },
    titleClass: { control: 'text' },
    subtitleClass: { control: 'text' },
    mainActionLabel: { control: 'text' },
    secondaryActionLabel: { control: 'text' },
    mainActionEvent: { control: 'function' },

  },
} as Meta;

const Template: Story<HeaderComponent> = (args: HeaderComponent) => ({
  component: HeaderComponent,
  props: args,
});

export const NoActions = Template.bind({});
NoActions.args = {
  title: 'Title of the screen',
  subtitle: 'No actions',
  mainActionLabel: `Primary`,
};

export const SimpleActions = Template.bind({});
SimpleActions.args = {
  title: 'Title of the screen',
  subtitle: 'Simple action',
  mainActionLabel: `Primary`,
  secondaryActionLabel: `Secondary`,
  mainActionEvent: (event: any) => (console.log('mainAction')),
};


