import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { LoadingComponent } from 'src/app/components';
import { LoadingModule } from 'projects/ng-utils/src/lib/loading/loading.module';

export default {
    title: 'Example/Loading',
    component: LoadingComponent,
    argTypes: {
        label: { control: 'text' },
        color: { control: 'color' },
    },
    decorators: [
        moduleMetadata({
            declarations: [],
            imports: [LoadingModule],
        }),
    ]
} as Meta;

const Template: Story<LoadingComponent> = (args: LoadingComponent) => ({
    component: LoadingComponent,
    props: args,
});

export const Spinner = Template.bind({});
Spinner.args = {
    label: '',
    color: 'primary'
};

export const Text = Template.bind({});
Text.args = {
    label: 'LOADING',
    color: 'black'
};

