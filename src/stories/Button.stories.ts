import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { ButtonComponent } from 'src/app/components';
import { ButtonModule } from 'projects/ng-utils/src/lib/button/button.module';

export default {
  title: 'Example/Button',
  component: ButtonComponent,
  argTypes: {
    primary: { control: 'boolean' },
    label: { control: 'text' },
  },
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [ButtonModule],
    }),
  ],
} as Meta;

const Template: Story<ButtonComponent> = (args: ButtonComponent) => ({
  component: ButtonComponent,
  props: args,
});

export const PrimaryColor = Template.bind({});
PrimaryColor.args = {
  primary: true,
  label: 'PrimaryButton',
};

export const Secondary = Template.bind({});
Secondary.args = {
  label: 'SecondaryButton',
};
