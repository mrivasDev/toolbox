import { Injectable } from '@angular/core';
import {
	HttpRequest,
	HttpHandler,
	HttpEvent,
	HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UtilService } from './util.service';
import { AuthService } from './auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
	constructor(
		private utilService: UtilService,
		private authService: AuthService
	) { }

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		const auth = this.utilService.getLS('auth', true);
		if (!auth) {
			return next.handle(request);
		}
		/* const diff = dayjs(auth.issued).add(auth.expires_in, 'second').diff(dayjs(), 'minute');
		if (diff > 0 && diff < 30) {
			this.authService.refresh().then(data => {
				this.utilService.saveAuth(data);
			});
		} */
		let headers = request.headers;
		if (request.url.includes('upload')) {
			headers = headers.set('Authorization', 'Bearer ' + auth.access_token)
				.append('Accept', 'application/json');
		} else {
			headers = headers.set('Authorization', 'Bearer ' + auth.access_token)
				.append('Content-Type', 'application/json')
				.append('Accept', 'application/json');
		}
		const requestClone = request.clone({
			headers
		});
		return next.handle(requestClone);
	}
}
