import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class UtilService {

	constructor(
		private router: Router,
		private snackBar: MatSnackBar,
		private matDialog: MatDialog
	) { }



	removeInvalidClasses() {
		setTimeout(() => {
			Array.from(document.querySelectorAll('mat-form-field')).forEach((formField) => {
				if (formField.className.includes('mat-form-field-invalid')) {
					formField.className = formField.className.replace('mat-form-field-invalid', '');
				}
			});
		}, 300);
	}

	/**
	 * Función que devuelve el padre más cercano filtrando por selector
	 * @param element Selector base
	 * @param parent Selector padre al que sea desea llegar
	 */
	parents(element: string | any, parentSelector: string) {
		let el: any = typeof element === 'string' ? document.querySelector(element) : element;
		const parents = Array.from(document.querySelectorAll(parentSelector));
		while (el && el.tagName !== 'HTML') {
			el = el.parentNode;
			if (parents.indexOf(el) !== -1) {
				return el;
			}
		}
		return null;
	}

	bloquearScroll(opened: boolean) {
		document.querySelector('html').setAttribute('style', opened ? 'hidden' : 'auto');
		if (opened) {
			this.smoothScroll();
		}
	}

	smoothScroll() {
		const currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
		if (currentScroll > 0) {
			window.requestAnimationFrame(this.smoothScroll);
			window.scrollTo(0, currentScroll - (currentScroll / 5));
		}
	}

	notification(message: string, action?: string, duration?: number) {
		if (!action) {
			action = 'Cerrar';
		}
		if (!duration) {
			duration = 5;
		}
		return this.snackBar.open(message, action, {
			duration: duration * 1000,
			panelClass: 'snack',
			verticalPosition: 'top',
			horizontalPosition: 'end'
		});
	}


	/**
	 *  Getter de localStorage
	 *  @param nombreVariable - Nombre de la variable
	 *  @param decodificar - true : json, false: string
	**/
	getLS(nombreVariable: string, decodificar?: boolean): any {
		let resultado = localStorage.getItem(nombreVariable);
		if (decodificar) {
			try {
				resultado = JSON.parse(resultado);
			} catch (e) {
				resultado = null;
			}
		}
		if (!resultado) {
			resultado = '';
		}
		return resultado;
	}
	/**
	 *  Getter de sessionStorage
	 *  @param nombreVariable - Nombre de la variable
	 *  @param decodificar - true : json, false: string
	**/
	getSS(nombreVariable: string, decodificar?: boolean): any {
		let resultado = sessionStorage.getItem(nombreVariable);
		if (decodificar) {
			try {
				resultado = JSON.parse(resultado);
			} catch (e) {
				resultado = null;
			}
		}
		if (!resultado) {
			resultado = '';
		}
		return resultado;
	}

	/**
	 *  Setter de localStorage
	 *  @param nombreVariable - Nombre de la variable
	 *  @param enJson - true : json, false: string
	 *  @param valor - Valor a guardar
	**/
	setLS(nombreVariable: string, valor: any, enJson?: boolean) {
		if (enJson) {
			valor = JSON.stringify(valor);
		}
		localStorage.setItem(nombreVariable, valor);
		return valor;
	}

	/**
	 *  Setter de sessionStorage
	 *  @param nombreVariable - Nombre de la variable
	 *  @param enJson - true : json, false: string
	 *  @param valor - Valor a guardar
	**/
	setSS(nombreVariable: string, valor: any, enJson?: boolean) {
		if (enJson) {
			valor = JSON.stringify(valor);
		}
		sessionStorage.setItem(nombreVariable, valor);
		return valor;
	}

	loseFocus() {
		setTimeout(() => {
			const el: any = document.querySelector(':focus');
			el.blur();
		}, 0);
	}

	haySubtablas() {
		return (document.querySelectorAll('.subtabla').length > 0) && !(Array.from(document.querySelectorAll('.subtabla')).filter(function () { return getComputedStyle(this.parentNode).display == 'flex'; }).length == 0);
	}

	abrirUrl(url) {
		window.open(environment.baseUrl + url, '_blank');
	}

	tieneClase(elem, clase) {
		return document.querySelector(elem).className.includes(clase);
	}

	/**
	 *  Ejemplo de uso:
	 *
	 *  this.utilService.blobToBase64(blob).then(base64 => {
	 *      console.log(base64);
	 *  }, error => {
	 *      this.utilService.notification({message: 'No se pudo procesar el archivo', type: 'error'});
	 *  });
	 */
	blobToBase64(blob: Blob): Promise<string> {
		return new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.onload = () => {
				const dataUrl = reader.result as string;
				const base64 = dataUrl.split(',')[1];
				resolve(base64);
			};
			reader.onerror = () => {
				reject();
			};
			reader.readAsDataURL(blob);
		});
	}

	base64ToFile(data, filename) {

		const arr = data.split(',');
		const mime = arr[0].match(/:(.*?);/)[1];
		const bstr = atob(arr[1]);
		let n = bstr.length;
		let u8arr = new Uint8Array(n);

		while (n--) {
			u8arr[n] = bstr.charCodeAt(n);
		}

		return new File([u8arr], filename, { type: mime });
	}


	groupBy(array, key) {
		return array
			.reduce((hash, obj) => {
				if (obj[key] === undefined) return hash;
				return Object.assign(hash, { [obj[key]]: (hash[obj[key]] || []).concat(obj) })
			}, {})
	}

	formatDate(date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('-');
	}

}
