import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { HeaderComponent } from 'projects/ng-utils/src/lib/header/header.component';
import { ButtonComponent } from 'projects/ng-utils/src/lib/button/button.component';
import { LoadingComponent } from 'projects/ng-utils/src/lib/loading/loading.component';
import { TableComponent } from 'projects/ng-utils/src/lib/table/table.component';
import { SegmentComponent } from './components';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ButtonComponent,
    LoadingComponent,
    SegmentComponent,
    TableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
