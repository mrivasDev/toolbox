import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SegmentComponent } from './segment/segment.component';
import { ButtonModule } from './button/button.module';
import { TableModule } from './table/table.module';
import { LoadingModule } from './loading/loading.module';
import { HeaderModule } from './header/header.module';
import { SegmentModule } from './segment/segment.module';

@NgModule({
	declarations: [],
	imports: [
		CommonModule,
		TableModule,
		LoadingModule,
		HeaderModule,
		ButtonModule,
		SegmentModule
	],
	exports: [
		TableModule,
		LoadingModule,
		HeaderModule,
		ButtonModule,
		SegmentModule
	],
	providers: [

	]
})
export class ComponentsModule { }
