import { Component, Inject, } from '@angular/core';
import {MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpService, UtilService } from '../../../../../src/app/core';

@Component({
    selector: 'ng-main-action-bottom-sheet',
    templateUrl: './mainActionBottomSheet.component.html',
    styleUrls: ['./mainActionBottomSheet.component.scss']
})
export class MainActionBottomSheetComponent {

    public title: string = '';
    public subtitle: string = '';
    public apiUrl: string = '';
    public form: any[] = [];
    public formGroup: any = null;
    public method: string = '';

    public loading: boolean = false;

    constructor(
        private bottomSheetRef: MatBottomSheetRef<MainActionBottomSheetComponent>,
		private httpService: HttpService,
		private utilService: UtilService,
        @Inject(MAT_BOTTOM_SHEET_DATA) public data: any
    ) {
        this.title = data.title;
        this.subtitle = data.subtitle;
        this.apiUrl = data.apiUrl;
        this.form = data.form;
        this.method = data.method;
        this.initForm();
    }

    private initForm() {
        const formObj = this.form.reduce((accumulatedElement, {
            field, required
        }) => {
            accumulatedElement[field] = required
                ? new FormControl(null, Validators.required)
                : new FormControl(null);
            return accumulatedElement;
        }, {});
        this.formGroup = new FormGroup(formObj);
    }

    public handleSectorChange(event, element) {
        this.formGroup.patchValue({
            [element.field]: event.value
        });
    }

    public onSubmit(event) {
        if (this.formGroup.valid) {
            this.loading = true;
            this.form.forEach(({ field, display }) => {
                if (display === 'datepicker') {
                    const parsedObject = {};
                    parsedObject[field] = this.utilService.formatDate(this.formGroup.value[field]);
                    this.formGroup.patchValue(parsedObject);
                }
            })
            this.httpService.post(this.apiUrl, this.formGroup.value).then(({ success, message }) => {
                this.loading = false;
                if (success) {
                    this.utilService.notification(message, '', 3);
                    this.formGroup.reset();
                    this.close();
                }
            }).catch((error) => {
                this.loading = false;
                console.error(error);
            });
        }
    }

    public close(): void {
        this.bottomSheetRef.dismiss();
    }
}


